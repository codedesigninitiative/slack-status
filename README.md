# Change your Slack status from the terminal

- Install [Slack CLI](https://github.com/rockymadden/slack-cli#installation)
- Edit your `.bashrc` or `.zshrc` (see config below)
- `source .bashrc`/`.zshrc`
- Run `green`/`yellow`/`red`/`other` in terminal


## Bash/Zsh config

```bash
alias green='slack status edit --text="Answer <5min" --emoji=":talktome:" && slack presence active'
alias yellow='slack status edit --text="Selective answer >5min" --emoji=":onlyurgent:" && slack presence active'
alias red='f() { slack status edit --text="Answer >1h" --emoji=":nointerruption:" && slack presence away && slack snooze start --minutes="${1-60}" }; f'
alias otherwork='f() { slack status edit --text="Not working for XYZ right now." --emoji=":otherwork:" && slack presence away && slack snooze start --minutes="${1-60}" }; f'
```
